﻿using System;
using System.Collections.Generic;
namespace Application
{
	public abstract class note
	{
		public abstract string WriteInfo();
	}

	public class octave : note
	{
		public override string WriteInfo() //проверяю подходит октава или не, то возвращаю пустоту, чтобы ее в будущий лист и записать
		{
			Console.WriteLine("Enter note`s octave");
			string octave = Console.ReadLine();
			int octaveint = Convert.ToInt32(octave);
			if (octaveint >= 0 && octaveint <= 8)
				return octave;
			else
				Console.WriteLine("Такой октавы не существует, попробуйте еще раз");
			return null;
		
		}
	}
	public class pitch : note
	{
		public override string WriteInfo() //проверяю подходит питч или не, если не, то возвращаю пустоту, чтобы ее в будущий лист и записать
		{
			Console.WriteLine("Enter pitch");
			string[] PosPitches = { "c", "d", "e", "f", "g", "a", "b", "c#", "d#", "e#", "f#", "g#", "a#", "b#" };
			string note = Console.ReadLine();
			bool note1 = false;
			for (int i = 0; i < PosPitches.Length; i++)
			{
				if (note == PosPitches[i])
				{
					note1 = true;
					Console.WriteLine("существует");
					break;
				}
				else
					continue;
			}
			if (note1 == true)
				return note;
			else
				return null;
		}
	}
	public class lists
	{
		List<string> octaves1 = new List<string>();
        
	}
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Possible notes: \"c\", \"d\", \"e\", \"f\", \"g\", \"a\", \"b\", \"c#\", \"d#\", \"e#\", \"f#\", \"g#\", \"a#\", \"b#\" \nPossible pitches: 0-8 ");
			note pitch1 = new pitch();
			note octave1 = new octave();

			pitch1.WriteInfo();
			octave1.WriteInfo();

		}
	}
}
